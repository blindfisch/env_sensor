# Environment-Sensor

Using the esp8266, this RTOS-based project reads environmental data from the
[bme280](https://github.com/BoschSensortec/BME280_driver) sensor and delivers it via MQTT-Protocol.

### Features

* ntp to initialize rtc

### what you need

This project uses the Integrated Development Framework from
[ESP8266 RTOS SDK](https://github.com/espressif/ESP8266_RTOS_SDK).

### Configuration

Configuration is done with the IDF.

```shell
idf.py menuconfig
	Application ->
		Wifi SSID
		Wifi Password
		NTP-Server (pool.ntp.org)
		Broker-URL (mqtt://test.mosquitto.org)
		Broker Topic (test)
```
```
mosquitto_sub -h test.mosquitto.org -t 'test' -F "%J" | jq
{
  "tst": "2022-11-10T22:38:36.023029Z+0100",
  "topic": "test/temperature",
  "qos": 0,
  "retain": 0,
  "payloadlen": 47,
  "payload": {
    "tst": 1668116315,
    "value": 21,
    "unit": "°C"
  }
}
{
  "tst": "2022-11-10T22:38:36.029271Z+0100",
  "topic": "test/pressure",
  "qos": 0,
  "retain": 0,
  "payloadlen": 49,
  "payload": {
    "tst": 1668116315,
    "value": 102266,
    "unit": "Pa"
  }
}
{
  "tst": "2022-11-10T22:38:36.035089Z+0100",
  "topic": "test/humidity",
  "qos": 0,
  "retain": 0,
  "payloadlen": 46,
  "payload": {
    "tst": 1668116315,
    "value": 62,
    "unit": "%"
  }
```
### TODO

* smart-config
