#include "esp_log.h"
#include "esp_wifi.h"
#include "lwip/apps/sntp.h"
#include "app_main.h"

static const char* TAG = "wifi";
#define YEAR2000 946684800UL

static int8_t app_sntp_init(app_context_t *ctx)
{
    xEventGroupWaitBits(ctx->event_group, WIFI_CONNECTED_BIT, false, true, portMAX_DELAY);
    sntp_setservername(0, CONFIG_NTP_SERVER);
    sntp_init();
    // wait for time to be set
    time_t now            = 0;
    int retry             = 0;
    const int retry_count = 10;
    while (now < YEAR2000 && ++retry < retry_count) {
        vTaskDelay(2000 / portTICK_PERIOD_MS);
        time(&now);
    }
    xEventGroupSetBits(ctx->event_group, VALID_TIME_BIT);
    ESP_LOGI(TAG, "valid time");
    return ESP_OK;
}

static esp_err_t system_event_handler(void *context, system_event_t *event)
{
    app_context_t *ctx = context;

    switch (event->event_id) {
    case SYSTEM_EVENT_STA_START:
        tcpip_adapter_set_hostname(TCPIP_ADAPTER_IF_STA, "sensor");
        esp_wifi_set_protocol(ESP_IF_WIFI_STA, WIFI_PROTOCOL_11B | WIFI_PROTOCOL_11G | WIFI_PROTOCOL_11N);
        esp_wifi_connect();
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
        xEventGroupSetBits(ctx->event_group, WIFI_CONNECTED_BIT);
        ESP_LOGI(TAG, "connected");
        app_sntp_init(ctx);
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        xEventGroupClearBits(ctx->event_group, WIFI_CONNECTED_BIT);
        ESP_LOGI(TAG, "disconnected");
        esp_wifi_connect();
        break;
    default:
        break;
    }
    return ESP_OK;
}

void app_wifi_init(app_context_t *ctx)
{
    ESP_ERROR_CHECK(esp_event_loop_init(system_event_handler, ctx));
    tcpip_adapter_init();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    wifi_config_t wifi_config = {
        .sta =
            {
                .ssid     = CONFIG_WIFI_SSID,
                .password = CONFIG_WIFI_PASSWORD,
            },
    };
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());
    ESP_ERROR_CHECK(esp_wifi_set_ps(WIFI_PS_MIN_MODEM));
}
