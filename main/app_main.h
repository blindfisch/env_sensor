#pragma once

#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/queue.h"

static const int WIFI_CONNECTED_BIT = BIT0;
static const int MQTT_CONNECTED_BIT = BIT1;
static const int VALID_TIME_BIT     = BIT2;
static const int SENSOR_READY_BIT   = BIT3;

typedef struct {
    EventGroupHandle_t event_group;
    QueueHandle_t data_queue;
} app_context_t;
