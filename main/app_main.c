#include "sdkconfig.h"

#include "nvs_flash.h"
#include "esp_log.h"
#include "driver/gpio.h"
#include "sys/time.h"

#include "app_main.h"
#include "app_mqtt.h"
#include "app_wifi.h"
#include "d1mini.h"

#include "sensor_bme280.h"


static const char* TAG = "main";

static app_context_t ctx;
struct sensor_t bme280_sensor;

static void task_heartbeat(void *params)
{
    gpio_config_t led;
    led.pin_bit_mask = BIT(LED_BUILTIN);
    led.mode         = GPIO_MODE_OUTPUT;
    led.intr_type    = GPIO_INTR_DISABLE;
    led.pull_up_en   = GPIO_PULLUP_DISABLE;
    led.pull_down_en = GPIO_PULLDOWN_DISABLE;

    gpio_config(&led);
    for (;;) {
        for (int i = 0; i <= 3; i++) {
            gpio_set_level(LED_BUILTIN, i % 2);
            vTaskDelay(100 / portTICK_PERIOD_MS);
        }
        vTaskDelay(4600 / portTICK_PERIOD_MS);
    }
}

static int8_t app_sensor_init(app_context_t *ctx)
{
    int8_t rslt;
    rslt = sensor_bme280_init(&bme280_sensor);
    if (rslt == ESP_OK) {
        xEventGroupSetBits(ctx->event_group, SENSOR_READY_BIT);
        ESP_LOGI(TAG, "sensor ready");
    }
    else
        ESP_LOGE(TAG, "app_sensor_init: %d", rslt);
    return rslt;
}

static void task_main_loop(void *params)
{
    int8_t rslt;
    app_context_t *ctx = params;
    struct sensor_data data[3];
    TickType_t xLastWakeTime    = xTaskGetTickCount();
    const TickType_t xFrequency = 60 * 1000 / portTICK_PERIOD_MS; // TODO Config

    for (;;) {
        xEventGroupWaitBits(ctx->event_group, SENSOR_READY_BIT | VALID_TIME_BIT, false, true, portMAX_DELAY);
        rslt = bme280_sensor.read(bme280_sensor.dev, data);
        if (rslt == ESP_OK) {
            for (int i = 0; i < 3; i++) {
                xQueueSendToBack(ctx->data_queue, &data[i], 0);
            }
        } else {
            ESP_LOGE(TAG, "bme20_sensor_read failure: %d", rslt);
        }

		vTaskDelayUntil(&xLastWakeTime, xFrequency);
    }
}

void app_main()
{
    uint8_t rslt = ESP_OK;
    nvs_flash_init();

    xTaskCreate(task_heartbeat, "heartbeat", 1024, NULL, 1, NULL);

    ctx.event_group = xEventGroupCreate();
    ctx.data_queue  = xQueueCreate(10, sizeof(struct sensor_data));

    app_wifi_init(&ctx);
    app_mqtt_init(&ctx);
    rslt |= app_sensor_init(&ctx);

    if (rslt == ESP_OK)
        xTaskCreate(task_main_loop, TAG, 2048, &ctx, 10, NULL);
    else
        ESP_LOGE(TAG, "unable to initialize");
}
