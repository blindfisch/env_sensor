#include "math.h"

#include "esp_log.h"
#include "mqtt_client.h"

#include "sensor.h"
#include "app_mqtt.h"

static const char* TAG = "mqtt";

static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event)
{
    app_context_t *ctx = event->user_context;

    switch (event->event_id) {
    case MQTT_EVENT_CONNECTED:
        xEventGroupSetBits(ctx->event_group, MQTT_CONNECTED_BIT);
        ESP_LOGI(TAG, "client connected");
        break;
    case MQTT_EVENT_DISCONNECTED:
        xEventGroupClearBits(ctx->event_group, MQTT_CONNECTED_BIT);
        ESP_LOGI(TAG, "client disconnected");
        break;
    default:
        break;
    }
    return ESP_OK;
}

static void task_mqtt(void *params)
{
    app_context_t *ctx = params;
    esp_mqtt_client_handle_t client;
    esp_mqtt_client_config_t mqtt_cfg = {
        .uri          = CONFIG_MQTT_BROKER_URL, //
        .event_handle = mqtt_event_handler,     //
        .user_context = ctx                     //
    };
    EventBits_t uxBits;

    uxBits = xEventGroupWaitBits(ctx->event_group, WIFI_CONNECTED_BIT, false, true, portMAX_DELAY);
    if((uxBits & WIFI_CONNECTED_BIT) == WIFI_CONNECTED_BIT) {
        client = esp_mqtt_client_init(&mqtt_cfg);
        if (client) {
            esp_mqtt_client_start(client);
        }

        struct sensor_data message;
        char topic[80];
        char msg[80];
        for (;;) {
            if (xQueueReceive(ctx->data_queue, &message, portMAX_DELAY)) {
                struct metric_label_t metric = metric_label[message.metric];
                snprintf(topic, 80, "%s/%s", CONFIG_MQTT_BROKER_TOPIC, metric.name);
                snprintf(msg, 80, "{\"tst\":%ld, \"value\":%d, \"unit\":\"%s\"}",
                    message.time, message.value, metric.unit);
                uxBits = xEventGroupWaitBits(ctx->event_group,
                    WIFI_CONNECTED_BIT | MQTT_CONNECTED_BIT, false, true, portMAX_DELAY);
                if((uxBits & (WIFI_CONNECTED_BIT | MQTT_CONNECTED_BIT)) == (WIFI_CONNECTED_BIT | MQTT_CONNECTED_BIT)) {
                    esp_mqtt_client_publish(client, topic, msg, 0, 1, 0);
                } else {
                    esp_mqtt_client_start(client);
                }
            }
        }
    }
}

void app_mqtt_init(app_context_t *ctx)
{
    xTaskCreate(task_mqtt, "mqtt", 2048, ctx, 9, NULL);
}
