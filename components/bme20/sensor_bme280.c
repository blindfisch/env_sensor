#include "math.h"

#include "driver/i2c.h"
#include "task.h"
#include "time.h"
#include "esp_log.h"
#include "bme280.h"

#include "d1mini.h"
#include "sensor_bme280.h"

#define SDA_PIN D2
#define SCL_PIN D1

static const uint8_t BME280_ADDR = BME280_I2C_ADDR_PRIM;
static const char* TAG = "sensor_bme280";

/*!
 * @brief This function provides the delay for required time (Microseconds) as per the input provided in some of the
 * APIs
 */
void user_delay_us(uint32_t period, void *intf_ptr)
{
    vTaskDelay(period / 1000 / portTICK_PERIOD_MS);
}

/*!
 * @brief This function reading the sensor's registers through I2C bus.
 */
static int8_t user_i2c_read(uint8_t reg_addr, uint8_t *reg_data, uint32_t len, void *intf_ptr)
{
    uint8_t dev_id = *(uint8_t *) intf_ptr;

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (dev_id << 1) | I2C_MASTER_WRITE, true);
    i2c_master_write_byte(cmd, reg_addr, true);

    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (dev_id << 1) | I2C_MASTER_READ, true);
    if (len > 1)
        i2c_master_read(cmd, reg_data, len - 1, I2C_MASTER_ACK);
    i2c_master_read_byte(cmd, reg_data + len - 1, I2C_MASTER_NACK);
    i2c_master_stop(cmd);

    esp_err_t err = i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_PERIOD_MS);
    i2c_cmd_link_delete(cmd);

    return (err == ESP_OK) ? BME280_OK : BME280_E_COMM_FAIL;
}

/*!
 * @brief This function for writing the sensor's registers through I2C bus.
 */
static int8_t user_i2c_write(uint8_t reg_addr, const uint8_t *reg_data, uint32_t len, void *intf_ptr)
{
    uint8_t dev_id = *(uint8_t *) intf_ptr;

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (dev_id << 1) | I2C_MASTER_WRITE, true);
    i2c_master_write_byte(cmd, reg_addr, true);
    i2c_master_write(cmd, (uint8_t *)reg_data, len, true);
    i2c_master_stop(cmd);

    esp_err_t err = i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_PERIOD_MS);
    i2c_cmd_link_delete(cmd);

	return (err == ESP_OK) ? BME280_OK : BME280_E_COMM_FAIL;
}

static void i2c_master_init()
{
    i2c_config_t i2c_config = {
        .mode             = I2C_MODE_MASTER,
        .sda_io_num       = SDA_PIN,
        .scl_io_num       = SCL_PIN,
        .sda_pullup_en    = GPIO_PULLUP_ENABLE,
        .scl_pullup_en    = GPIO_PULLUP_ENABLE,
        .clk_stretch_tick = 1000000 //
    };
    i2c_driver_install(I2C_NUM_0, I2C_MODE_MASTER);
    i2c_param_config(I2C_NUM_0, &i2c_config);
}

static int8_t sensor_bme280_read(void *dev_ptr, struct sensor_data data[3])
{
    int8_t rslt;
    struct bme280_data comp_data;
    struct bme280_dev *dev = dev_ptr;
    time_t now;

    rslt = bme280_set_sensor_mode(BME280_FORCED_MODE, dev);
    if (rslt == BME280_OK) {
        dev->delay_us(40*1000, dev->intf_ptr);
        time(&now);
        rslt = bme280_get_sensor_data(BME280_ALL, &comp_data, dev);
        if (rslt == BME280_OK) {
            data[0].time   = now;
            data[0].metric = TEMPERATURE;
            data[0].value  = round(0.01f * comp_data.temperature);
            data[1].time   = now;
            data[1].metric = HUMIDITY;
            data[1].value  = round(1.0f / 1024.0f * comp_data.humidity);
            data[2].time   = now;
            data[2].metric = PRESSURE;
            data[2].value  = comp_data.pressure;
        }
    }
    return rslt;
}

int8_t sensor_bme280_init(struct sensor_t *sensor)
{
    int8_t rslt = 0;
    struct bme280_dev *dev = malloc(sizeof(struct bme280_dev));

    dev->intf     = BME280_I2C_INTF;
    dev->delay_us = user_delay_us;
    dev->read     = user_i2c_read;
    dev->write    = user_i2c_write;
    dev->intf_ptr = (void *) &BME280_ADDR;

    i2c_master_init();

    if ((rslt = bme280_init(dev)) != BME280_OK) goto err;
    /* Recommended mode of operation: Weather */
    dev->settings.osr_h  = BME280_OVERSAMPLING_1X;
    dev->settings.osr_p  = BME280_OVERSAMPLING_1X;
    dev->settings.osr_t  = BME280_OVERSAMPLING_1X;
    dev->settings.filter = BME280_FILTER_COEFF_OFF;
    uint8_t settings_sel = BME280_OSR_PRESS_SEL | BME280_OSR_TEMP_SEL | BME280_OSR_HUM_SEL | BME280_FILTER_SEL;
    if ((rslt = bme280_set_sensor_settings(settings_sel, dev)) != BME280_OK) goto err;
    sensor->dev  = dev;
    sensor->read = sensor_bme280_read;
    goto rslt;
err:
    ESP_LOGE(TAG, "sensor_bme280_init failed :%d", rslt);
    free(dev);
rslt:
    return rslt;
}
