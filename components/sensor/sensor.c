#include "sensor.h"

const struct metric_label_t metric_label[] = {{"temperature", "°C"}, {"humidity", "%"}, {"pressure", "Pa"}};
