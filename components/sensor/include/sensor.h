#ifndef SENSOR_H
#define SENSOR_H
#include "esp_system.h"

struct metric_label_t {
    const char *name;
    const char *unit;
};
extern const struct metric_label_t metric_label[];

enum metric_t {TEMPERATURE, HUMIDITY, PRESSURE};

struct sensor_data {
    time_t        time;
    enum metric_t metric;
    int32_t       value;
};

struct sensor_t {
    void   *dev;
    int8_t (*read)(void *dev, struct sensor_data data[3]);
};
#endif
